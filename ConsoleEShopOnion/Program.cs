﻿using System;

namespace ConsoleEShopOnion
{
    /// <summary>
    /// Main class of the program
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Start of the program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            menu.MenuInterraction();
            GC.KeepAlive(OA_DataAccess.Product.listOfProducts);
            GC.KeepAlive(OA_DataAccess.Order.ordersList);
            GC.KeepAlive(OA_DataAccess.User.Guest.people);
        }
    }
}
