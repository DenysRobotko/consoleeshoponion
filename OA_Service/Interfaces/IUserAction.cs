﻿using OA_DataAccess;

namespace OA_Service.Interfaces
{
    public interface IUserAction
    {
        /// <summary>
        /// Make a new order
        /// </summary>
        /// <param name="number">Number of product to buy</param>
        /// <param name="choose">Confirm status
        /// 1 - Confirmed
        /// 2 - Canseled</param>
        public void NewOrder(int number, int choose);

        /// <summary>
        /// Change status of your order to received
        /// </summary>
        public void SetOrderStatusReceived();

        /// <summary>
        /// Shows history of all orders by user
        /// </summary>
        public void ShowHistory();

        /// <summary>
        /// Changes personal information
        /// </summary>
        /// <param name="newName">New login</param>
        /// <param name="newPassword">New password</param>
        public void ChangePersonalData(string newName, string newPassword);

        /// <summary>
        /// Signing out from your account
        /// </summary>
        public void SignOut();

        /// <summary>
        /// Shows product by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name);

        /// <summary>
        /// Shows list of all products in the shop
        /// </summary>
        public void ShowProducts();

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>
        public void SignIn(string name, string password, int number);

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password);
    }
}
