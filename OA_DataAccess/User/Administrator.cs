﻿namespace OA_DataAccess.User
{
    /// <summary>
    /// Describe person, authorized as an administrator
    /// </summary>
    public class Administrator : Guest
    {
        /// <summary>
        /// Constructor to add new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public Administrator(string name, string password)
        {
            Nickname = name;
            Password = password;
        }
    }
}
