﻿using OA_DataAccess;
using System.Collections.Generic;

namespace OA_Repository
{
    /// <summary>
    /// Interface to wokr with orders
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Returns all orders in new collection
        /// </summary>
        /// <returns></returns>
        public List<Order> GetAllOrders();
    }
}
