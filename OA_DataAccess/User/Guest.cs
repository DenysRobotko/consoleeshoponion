﻿using System.Collections.Generic;

namespace OA_DataAccess.User
{
    /// <summary>
    /// Descride not authorized person
    /// </summary>
    public class Guest
    {
        /// <summary>
        /// Static list, that emulate data base
        /// </summary>
        public static List<Guest> people = new List<Guest>();

        /// <summary>
        /// Person name
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// Person password
        /// </summary>
        public string Password { get; set; }

        public Guest()
        {

        }

        /// <summary>
        /// Constructor to add new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public Guest(string name, string password)
        {
            Nickname = name;
            Password = password;
        }
    }
}
