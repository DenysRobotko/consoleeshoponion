﻿using OA_DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace OA_Repository
{
    /// <summary>
    /// Implements IOrderRepository
    /// Work with orders
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        /// <summary>
        /// Returns all orders in new collection
        /// </summary>
        /// <returns></returns>
        public List<Order> GetAllOrders()
        {
            var res = Order.ordersList;

            return res.ToList();
        }
    }
}
