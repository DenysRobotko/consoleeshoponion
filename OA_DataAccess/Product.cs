﻿using System.Collections.Generic;

namespace OA_DataAccess
{
    /// <summary>
    /// Describes products in shop
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Dscribes name of the product
        /// </summary>
        public string probuctName { get; set; }
        /// <summary>
        /// Dscribes category of the product
        /// </summary>
        public string productCategory { get; set; }
        /// <summary>
        /// Dscrption of the product
        /// </summary>
        public string productDescription { get; set; }
        /// <summary>
        /// Cost of the product
        /// </summary>
        public double productCost { get; set; }

        /// <summary>
        /// Static list, that emulate data base
        /// </summary>
        public static List<Product> listOfProducts = new List<Product>()
        {new Product("Galaxy s20", "Phone", "New flagship", 1200)};

        /// <summary>
        /// Constructtor to create new products
        /// </summary>
        /// <param name="name">Name of product</param>
        /// <param name="category">Category of product</param>
        /// <param name="description">Product Description</param>
        /// <param name="cost">Product Cost</param>
        public Product(string name, string category, string description, double cost)
        {
            probuctName = name;
            productCategory = category;
            productDescription = description;
            productCost = cost;
        }
        public Product()
        {

        }
    }
}
