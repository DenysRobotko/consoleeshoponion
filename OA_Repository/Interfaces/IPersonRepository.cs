﻿using OA_DataAccess.User;
using System.Collections.Generic;

namespace OA_Repository.Interfaces
{
    /// <summary>
    /// Interface to work with users
    /// </summary>
    public interface IPersonRepository
    {
        /// <summary>
        /// Returns all users in new collection
        /// </summary>
        /// <returns></returns>
        public List<Guest> GetAllGuests();
    }
}
