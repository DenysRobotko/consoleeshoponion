﻿using OA_DataAccess;

namespace OA_Service.Interfaces
{
    public interface IGuestAction
    {
        /// <summary>
        /// Find product in list of products by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name);
        
        /// <summary>
        /// Show list of all products
        /// </summary>
        public void ShowProducts();

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>

        public void SignIn(string name, string password, int number);

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password);
    }
}
