﻿using OA_DataAccess;

namespace OA_Service.Interfaces
{
    public interface IAdministratorAction
    {
        /// <summary>
        /// Shows product by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name);

        /// <summary>
        /// Shows list of all products in the shop
        /// </summary>
        public void ShowProducts();

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>
        public void SignIn(string name, string password, int number);

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password);

        /// <summary>
        /// Make a new order
        /// </summary>
        /// <param name="number">Number of product to buy</param>
        /// <param name="choose">Confirm status
        /// 1 - Confirmed
        /// 2 - Canseled</param>
        public void NewOrder(int number, int choose);

        /// <summary>
        /// Shows all registered users
        /// </summary>
        public void ShowUsers();

        /// <summary>
        /// Changes personal information of registered user
        /// </summary>
        /// <param name="newName">User name, to change information</param>
        public void ChangeUsersData(string newName);

        /// <summary>
        /// Adds new product to the shop
        /// </summary>
        /// <param name="name">Product name</param>
        /// <param name="category">Product category</param>
        /// <param name="description">Product description</param>
        /// <param name="cost">Product price</param>
        public void AddProduct(string name, string category, string description, double cost);

        /// <summary>
        /// Changes informtion abour product
        /// </summary>
        /// <param name="name">Name of the product</param>
        public void ChangeProductInformation(string name);

        /// <summary>
        /// Shows all orders
        /// </summary>
        public void ShowOrders();

        /// <summary>
        /// Shows all possible order statuses
        /// </summary>
        public void ShowOrderStatus();

        /// <summary>
        /// Changes order status
        /// </summary>
        /// <param name="personName">Name of the person</param>
        /// <param name="num">Number of the new status</param>
        public void ChangeOrderStatus(string personName, int num);

        /// <summary>
        /// Signing out from your account
        /// </summary>
        public void SignOut();
    }
}
