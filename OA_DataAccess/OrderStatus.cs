﻿namespace OA_DataAccess
{
    /// <summary>
    /// Enumarable of all possible order statuses
    /// </summary>
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Received,
        Completed,
        Canceled
    }
}
