using NUnit.Framework;
using OA_DataAccess;
using OA_DataAccess.User;
using OA_Repository;
using OA_Service.Implementation;
using System;

namespace ConsoleEShopOnion.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void GetDataReturnsRightOrdersCount()
        {
            UserAction u = new UserAction("q", "1");
            u.NewOrder(1, 1);
            OrderRepository rep = new OrderRepository();
            Assert.AreEqual(Order.ordersList.Count, rep.GetAllOrders().Count);
        }

        [TestCase("AAA", "letter", "zzz", 12)]
        public void GetDataReturnsRightProductsCount(string n, string c, string d, double p)
        {
            AdministratorAction a = new AdministratorAction("q", "1");
            a.AddProduct(n, c, d, p);
            ProductRepository rep = new ProductRepository();
            Assert.AreEqual(Product.listOfProducts.Count, rep.GetAllProducts().Count);
        }

        [TestCase("Kolya1", "qqqqq1", 1)]
        public void GetDataReturnsRightPeopleCount(string name, string password, int number)
        {
            GuestAction g = new GuestAction();
            g.SignIn(name, password, number);
            PersonRepository rep = new PersonRepository();
            Assert.AreEqual(Guest.people.Count, rep.GetAllGuests().Count);
        }

        [Test]
        public void GetDataReturnsRightOrders()
        {
            UserAction u = new UserAction("q", "1");
            OrderRepository rep = new OrderRepository();
            u.NewOrder(1, 1);
            Assert.AreEqual(Order.ordersList, rep.GetAllOrders());
        }

        [TestCase("Kolya2", "qqqqq1", 1)]
        public void GetDataReturnsRightUsers(string name, string password, int number)
        {
            GuestAction g = new GuestAction();
            PersonRepository rep = new PersonRepository();
            g.SignIn(name, password, number);
            Assert.AreEqual(Guest.people, rep.GetAllGuests());
        }

        [TestCase("AAAA", "letter", "zzz", 12)]
        public void GetDataReturnsRightProducts(string n, string c, string d, double p)
        {
            AdministratorAction a = new AdministratorAction("q", "1");
            ProductRepository rep = new ProductRepository();
            a.AddProduct(n, c, d, p);
            Assert.AreEqual(Product.listOfProducts, rep.GetAllProducts());
        }

        #region Guest Tests
        [TestCase("Galaxy s20", true)]
        public void GuestFindByProbuctReturnObject(string name, bool temp)
        {
            GuestAction g = new GuestAction();

            Assert.AreEqual(g.FindProductByName(name) is Product, temp);
        }

        [TestCase("Phone")]
        public void GuestFindByProbuctThrowsNullReferenceException(string name)
        {
            GuestAction g = new GuestAction();

            Assert.Throws<NullReferenceException>(() => g.FindProductByName(name));
        }

        [TestCase(null)]
        public void GuestFindByProbuctThrowsArgumentNullException(string name)
        {
            GuestAction g = new GuestAction();

            Assert.Throws<ArgumentNullException>(() => g.FindProductByName(name));
        }

        [TestCase(null, "Kolya", 1)]
        [TestCase("Kolya", null, 2)]
        public void GuestSignInThrowsArgumentNullException(string name, string password, int number)
        {
            GuestAction g = new GuestAction();

            Assert.Throws<ArgumentNullException>(() => g.SignIn(name, password, number));
        }
        [TestCase("Kolya", "123", 1)]
        public void GuestSignInAddNewPerson(string name, string password, int number)
        {
            GuestAction g = new GuestAction();
            int count = Guest.people.Count;
            g.SignIn(name, password, number);

            Assert.AreEqual(Guest.people.Count, count + 1);
        }

        [TestCase(null, "Kolya")]
        [TestCase("Kolya", null)]
        public void GuestLogInThrowsArgumentNullException(string name, string password)
        {
            GuestAction g = new GuestAction();

            Assert.Throws<ArgumentNullException>(() => g.LogIn(name, password));
        }
        #endregion

        #region User Tests
        [TestCase(0, 1)]
        public void UserNewOrderThrowsArgumentNullException(int number, int choose)
        {
            UserAction u = new UserAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => u.NewOrder(number, choose));
        }

        [TestCase(-1, 1)]
        public void UserNewOrderThrowsIndexOutOfRangeException(int number, int choose)
        {
            UserAction u = new UserAction("q", "1");

            Assert.Throws<IndexOutOfRangeException>(() => u.NewOrder(number, choose));
        }

        [TestCase(1, 1)]
        public void UserNewOrderAddsNewOrder(int number, int choose)
        {
            UserAction u = new UserAction("q", "1");
            int count = Order.ordersList.Count + 1;
            u.NewOrder(number, choose);

            Assert.AreEqual(Order.ordersList.Count, count);
        }

        [TestCase(null, "qwe")]
        [TestCase("Misha", null)]
        public void UserChangePersonalDataThrowsArgumentNullException(string n, string p)
        {
            UserAction u = new UserAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => u.ChangePersonalData(n, p));
        }
        #endregion

        #region Administrator Tests

        public void AdministratorNewOrderThrowsArgumentNullException(int number, int choose)
        {
            AdministratorAction u = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => u.NewOrder(number, choose));
        }

        [TestCase(-1, 1)]
        public void AdministratorNewOrderThrowsIndexOutOfRangeException(int number, int choose)
        {
            AdministratorAction u = new AdministratorAction("q", "1");

            Assert.Throws<IndexOutOfRangeException>(() => u.NewOrder(number, choose));
        }

        [TestCase(1, 1)]
        public void AdministratorNewOrderAddsNewOrder(int number, int choose)
        {
            AdministratorAction u = new AdministratorAction("q", "1");
            int count = Order.ordersList.Count + 1;
            u.NewOrder(number, choose);

            Assert.AreEqual(Order.ordersList.Count, count);
        }

        [TestCase(null)]
        public void AdministratorChangeUserDataThrowsArgumentNullException(string name)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => a.ChangeUsersData(name));
        }

        [TestCase(null, "letter", "zzz", 12)]
        [TestCase("a", null, "zzz", 12)]
        [TestCase("a", "letter", null, 12)]
        [TestCase("a", "letter", "zzz", 0)]
        public void AdministratorAddProductThrowsArgumentNullException(string n, string c, string d, double p)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => a.AddProduct(n, c, d, p));
        }

        [TestCase("Galaxy s20", "letter", "zzz", 12)]
        public void AdministratorAddProductThrowsArgumentException(string n, string c, string d, double p)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentException>(() => a.AddProduct(n, c, d, p));
        }

        [TestCase(null)]
        public void AdministratorChangeProductInformationThrowsArgumentNullException(string name)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => a.ChangeProductInformation(name));
        }

        [TestCase(null, 2)]
        public void AdministratorChangeOrderStatusThrowsArgumentNullException(string name, int num)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<ArgumentNullException>(() => a.ChangeOrderStatus(name, num));
        }

        [TestCase("q", 0)]
        public void AdministratorChangeOrderStatusThrowsIndexOutOfRangeException(string name, int num)
        {
            AdministratorAction a = new AdministratorAction("q", "1");

            Assert.Throws<IndexOutOfRangeException>(() => a.ChangeOrderStatus(name, num));
        }
        #endregion
    }
}