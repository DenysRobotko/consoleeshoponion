﻿using OA_DataAccess.User;
using System.Collections.Generic;

namespace OA_DataAccess
{
    /// <summary>
    /// Describe order
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Person, who make an order
        /// </summary>
        public Guest buyer { get; set; }

        /// <summary>
        /// Product, which are odreder
        /// </summary>
        public Product product { get; set; }

        /// <summary>
        /// Status of the order
        /// </summary>
        public OrderStatus status { get; set; }

        /// <summary>
        /// Static list, that emulate data base
        /// </summary>
        public static List<Order> ordersList = new List<Order>();
        
        /// <summary>
        /// Constructor to create new order
        /// </summary>
        /// <param name="buyer">Buyer name</param>
        /// <param name="product">Product to buy</param>
        public Order(Guest buyer, Product product)
        {
            this.buyer = buyer;
            this.product = product;
        }

        public Order()
        {

        }
    }
}
