﻿using OA_DataAccess;
using OA_DataAccess.User;
using OA_Repository;
using OA_Repository.Interfaces;
using OA_Service.Interfaces;
using System;
using System.Linq;

namespace OA_Service.Implementation
{
    public class UserAction:User, IUserAction
    {
        readonly IProductRepository productRepository = new ProductRepository();
        readonly IOrderRepository orderRepository = new OrderRepository();
        readonly IPersonRepository personRepository = new PersonRepository();
        
        /// <summary>
        /// Constructor to create new user
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        public UserAction(string n, string p) : base(n, p)
        {

        }

        /// <summary>
        /// Make a new order
        /// </summary>
        /// <param name="number">Number of product to buy</param>
        /// <param name="choose">Confirm status
        /// 1 - Confirmed
        /// 2 - Canseled</param>
        public void NewOrder(int number, int choose)
        {
            if (number == 0)
                throw new ArgumentNullException(nameof(number), "Parametr was null");
            if (number > 0 && number < productRepository.GetAllProducts().Count + 1)
            {
                Order newOrder = new Order(this, productRepository.GetAllProducts()[number - 1]);
                switch (choose)
                {
                    case 1:
                        newOrder.status = OrderStatus.New;
                        Order.ordersList.Add(newOrder);
                        break;
                    case 2:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        break;
                    default:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        Console.WriteLine("You have canceled your order");
                        break;
                }
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(number) + " was out of range");
                throw ex;
            }
        }

        /// <summary>
        /// Change status of your order to received
        /// </summary>
        public void SetOrderStatusReceived()
        {
            Order order = orderRepository.GetAllOrders().FindLast(x => x.buyer == this);
            Console.WriteLine("Have you received your order? y/n");
            string str = Console.ReadLine();
            if (str == "y")
            {
                order.status = OrderStatus.Received;
                Console.WriteLine("Status has changed");
            }
            else if (str != "y")
                Console.WriteLine("You have not changed order status");
        }

        /// <summary>
        /// Shows history of all orders by user
        /// </summary>
        public void ShowHistory()
        {
            if (this.Nickname == null)
            {
                Console.WriteLine("Log in to watch your history");
            }
            else
            {
                var resultedList = from orders in orderRepository.GetAllOrders()
                                   where orders.buyer == this
                                   select orders;
                foreach (var element in resultedList)
                {
                    Console.WriteLine($"{element.buyer.Nickname} Ordered {element.product.probuctName}. Now satus is {element.status}");
                }
            }
        }

        /// <summary>
        /// Changes personal information
        /// </summary>
        /// <param name="newName">New login</param>
        /// <param name="newPassword">New password</param>
        public void ChangePersonalData(string newName, string newPassword)
        {
            Nickname = newName ?? throw new ArgumentNullException(nameof(newName), "Parametr was null");
            this.Password = newPassword ?? throw new ArgumentNullException(nameof(newPassword), "Parametr was null");
        }

        /// <summary>
        /// Signing out from your account
        /// </summary>
        public void SignOut()
        {
            this.Nickname = null;
            this.Password = null;
        }

        /// <summary>
        /// Shows product by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (productRepository.GetAllProducts().Any(x => x.probuctName != name))
            {
                var ex = new NullReferenceException();
                throw ex;
            }
            Product element = productRepository.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"1. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        /// <summary>
        /// Shows list of all products in the shop
        /// </summary>
        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in productRepository.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>
        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (personRepository.GetAllGuests().Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (personRepository.GetAllGuests().Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }
    }
}
