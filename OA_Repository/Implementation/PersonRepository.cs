﻿using OA_DataAccess.User;
using OA_Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OA_Repository
{
    /// <summary>
    /// Implements IPersonRepository
    /// Work with users
    /// </summary>
    public class PersonRepository : IPersonRepository
    {
        /// <summary>
        /// Returns all users in new collection
        /// </summary>
        /// <returns></returns>
        public List<Guest> GetAllGuests()
        {
            var res = Guest.people;

            return res.ToList();
        }
    }
}
