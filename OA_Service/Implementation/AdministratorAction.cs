﻿using OA_DataAccess;
using OA_DataAccess.User;
using OA_Repository;
using OA_Repository.Interfaces;
using OA_Service.Interfaces;
using System;
using System.Linq;

namespace OA_Service.Implementation
{
    public class AdministratorAction : Administrator, IAdministratorAction
    {
        readonly IProductRepository productRepository = new ProductRepository();
        readonly IOrderRepository orderRepository = new OrderRepository();
        readonly IPersonRepository personRepository = new PersonRepository();

        /// <summary>
        /// Constructor to create new administrator
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        public AdministratorAction(string n, string p) : base(n, p)
        {

        }

        /// <summary>
        /// Shows product by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (productRepository.GetAllProducts().Any(x => x.probuctName != name))
            {
                var ex = new NullReferenceException();
                throw ex;
            }
            Product element = productRepository.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        /// <summary>
        /// Shows list of all products in the shop
        /// </summary>
        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in productRepository.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>
        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (personRepository.GetAllGuests().Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (personRepository.GetAllGuests().Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }

        /// <summary>
        /// Make a new order
        /// </summary>
        /// <param name="number">Number of product to buy</param>
        /// <param name="choose">Confirm status
        /// 1 - Confirmed
        /// 2 - Canseled</param>
        public void NewOrder(int number, int choose)
        {
            if (number == 0)
                throw new ArgumentNullException(nameof(number), "Parametr was null");
            Console.WriteLine("Select which product you want to chose:");
            if (number > 0 && number < productRepository.GetAllProducts().Count + 1)
            {
                Order newOrder = new Order(this, Product.listOfProducts[number - 1]);
                switch (choose)
                {
                    case 1:
                        newOrder.status = OrderStatus.New;
                        Order.ordersList.Add(newOrder);
                        break;
                    case 2:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        break;
                    default:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        Console.WriteLine("You have canceled your order");
                        break;
                }
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(number) + " was out of range");
                throw ex;
            }
        }

        /// <summary>
        /// Shows all registered users
        /// </summary>
        public void ShowUsers()
        {
            int i = 1;
            foreach (var element in personRepository.GetAllGuests())
                if (element is User)
                {
                    Console.WriteLine($"{i}. " + element.Nickname);
                    i++;
                }
        }

        /// <summary>
        /// Changes personal information of registered user
        /// </summary>
        /// <param name="newName">User name, to change information</param>
        public void ChangeUsersData(string newName)
        {
            if (newName == null)
                throw new ArgumentNullException(nameof(newName), "Parametr was null");
            ShowUsers();
            Console.WriteLine("Enter new name");
            string personName = Console.ReadLine();
            Guest guest = orderRepository.GetAllOrders().Find(x => x.buyer.Nickname == personName).buyer;
            if (guest != null)
            {
                guest.Nickname = newName;
            }
        }

        /// <summary>
        /// Adds new product to the shop
        /// </summary>
        /// <param name="name">Product name</param>
        /// <param name="category">Product category</param>
        /// <param name="description">Product description</param>
        /// <param name="cost">Product price</param>
        public void AddProduct(string name, string category, string description, double cost)
        {
            if (name == null || category == null || description == null || cost == 0)
                throw new ArgumentNullException(nameof(name));
            if (productRepository.GetAllProducts().Any(x => x.probuctName == name))
                throw new ArgumentException(nameof(name) + " already exists");
            Product.listOfProducts.Add(new Product(name, category, description, cost));
        }

        /// <summary>
        /// Changes informtion abour product
        /// </summary>
        /// <param name="name">Name of the product</param>
        public void ChangeProductInformation(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            ShowProducts();
            if (productRepository.GetAllProducts().Any(x => x.probuctName == name))
            {
                Console.WriteLine("Enter new category");
                this.FindProductByName(name).productCategory = Console.ReadLine();
                Console.WriteLine("Enter new description");
                this.FindProductByName(name).productDescription = Console.ReadLine();
                Console.WriteLine("Enter new price");
                double.TryParse(Console.ReadLine(), out double number);
                FindProductByName(name).productCost = number;
            }
        }

        /// <summary>
        /// Shows all orders
        /// </summary>
        public void ShowOrders()
        {
            foreach (var order in orderRepository.GetAllOrders())
                Console.WriteLine($"{order.buyer.Nickname} ordered {order.product.probuctName} with status {order.status}");
        }

        /// <summary>
        /// Shows all possible order statuses
        /// </summary>
        public void ShowOrderStatus()
        {
            int i = 0;
            foreach (var elem in Enum.GetNames(typeof(OrderStatus)))
            {
                i++;
                Console.WriteLine(i + ". " + elem);
            }
        }

        /// <summary>
        /// Changes order status
        /// </summary>
        /// <param name="personName">Name of the person</param>
        /// <param name="num">Number of the new status</param>
        public void ChangeOrderStatus(string personName, int num)
        {
            if (personName == null)
                throw new ArgumentNullException(nameof(personName), "Parametr was null");
            Order singleOrder = orderRepository.GetAllOrders().Find(x => x.buyer.Nickname == personName);

            if (num > 0 && num < Enum.GetNames(typeof(OrderStatus)).Length + 1)
            {
                singleOrder.status = (OrderStatus)Enum.GetValues(typeof(OrderStatus)).GetValue(num - 1);
                Console.WriteLine("Status has changed");
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(num) + " was out of range");
                throw ex;
            }
        }

        /// <summary>
        /// Signing out from your account
        /// </summary>
        public void SignOut()
        {
            this.Nickname = null;
            this.Password = null;
        }
    }
}
