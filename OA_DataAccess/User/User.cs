﻿namespace OA_DataAccess.User
{
    /// <summary>
    /// Describe person, authorized as an user
    /// </summary>
    public class User : Guest
    {
        /// <summary>
        /// Constructor to add new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public User(string name, string password)
        {
            Nickname = name;
            Password = password;
        }   
    }
}
