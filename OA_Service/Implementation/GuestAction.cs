﻿using OA_DataAccess;
using OA_DataAccess.User;
using OA_Service.Interfaces;
using System;
using System.Linq;
using OA_Repository.Interfaces;
using OA_Repository;

namespace OA_Service.Implementation
{
    public class GuestAction:Guest, IGuestAction
    {
        readonly IProductRepository productRepository = new ProductRepository();
        
        /// <summary>
        /// Initialize new guest
        /// </summary>
        public GuestAction()
        {
        }

        /// <summary>
        /// Find product in list of products by its name
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <returns></returns>
        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            Product element = productRepository.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"1. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        /// <summary>
        /// Show list of all products
        /// </summary>
        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in productRepository.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        /// <summary>
        /// Authorize as a new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        /// <param name="number">Choose how to authorize
        /// 1 - as an User
        /// 2 - as an Administrator</param>
        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (Guest.people.Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                Guest.people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        /// <summary>
        /// Log in for already registered user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="password">User password</param>
        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (Guest.people.Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }
    }
}
