﻿using OA_DataAccess;
using OA_Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace OA_Repository
{
    /// <summary>
    /// Implements IProductRepository
    /// Work with products
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        /// <summary>
        /// Returns all products in new collection
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProducts()
        {
            var res = Product.listOfProducts;

            return res.ToList();
        }
    }
}
