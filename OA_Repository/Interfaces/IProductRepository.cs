﻿using OA_DataAccess;
using System.Collections.Generic;

namespace OA_Repository.Interfaces
{
    /// <summary>
    /// Interface to work with products
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Returns all products in new collection
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProducts();
    }
}
